plugins {
    id("com.android.application")
    id("kotlin-android")
}

android {
    compileSdk = 30
    buildToolsVersion = "30.0.3"

    defaultConfig {
        applicationId = "com.triPCups.android.igTAGzManager"
        minSdk = 21
        targetSdk = 30
        versionCode = 2
        versionName = "1.1"

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }

    buildTypes {
        release {
            isMinifyEnabled = false
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    kotlinOptions {
        jvmTarget = "1.8"
    }
    buildFeatures {
        viewBinding = true
    }
}

dependencies {

    implementation("androidx.core:core-ktx:1.6.0")
    implementation("androidx.appcompat:appcompat:1.3.1")
    implementation("com.google.android.material:material:1.4.0")
    implementation("androidx.constraintlayout:constraintlayout:2.1.0")
    implementation("androidx.lifecycle:lifecycle-livedata-ktx:2.3.1")
    implementation("androidx.lifecycle:lifecycle-viewmodel-ktx:2.3.1")
    implementation("androidx.navigation:navigation-fragment-ktx:2.3.5")
    implementation("androidx.navigation:navigation-ui-ktx:2.3.5")
    implementation("androidx.preference:preference:1.1.1")
    testImplementation("junit:junit:4.13.2")
    androidTestImplementation("androidx.test.ext:junit:1.1.3")
    androidTestImplementation("androidx.test.espresso:espresso-core:3.4.0")

    //flexbox layout manager
    implementation("com.google.android.flexbox:flexbox:3.0.0")

    //story-like profile view
    implementation("com.qintong:insLoadingAnimation:1.1.0")

    //kotlin lib for image loading
    implementation("io.coil-kt:coil:1.3.2")


    //koin dependency injection
    // Koin core features
    implementation("io.insert-koin:koin-core:3.1.2")
// Koin test features
    testImplementation("io.insert-koin:koin-test:3.1.2")
// Koin main features for Android (Scope,ViewModel ...)
    implementation("io.insert-koin:koin-android:3.1.2")
// Koin Java Compatibility
    implementation("io.insert-koin:koin-android-compat:3.1.2")
// Koin for Jetpack WorkManager
    implementation("io.insert-koin:koin-androidx-workmanager:3.1.2")


    //gson
    implementation("com.google.code.gson:gson:2.8.6")

}