package com.triPCups.android.igTAGzManager.ui.categories

import android.os.Bundle
import android.transition.TransitionInflater
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.triPCups.android.igTAGzManager.R
import com.triPCups.android.igTAGzManager.adapters.TagsCategoriesAdapter
import com.triPCups.android.igTAGzManager.data.getGeneralTags
import com.triPCups.android.igTAGzManager.data.getIsGeneralTagsEnabled
import com.triPCups.android.igTAGzManager.databinding.FragmentCategoriesBinding
import com.triPCups.android.igTAGzManager.ui.more.MoreViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class CategoriesFragment : Fragment() {

    val homeViewModel: CategoriesViewModel by viewModel()

    private val generalTags: String
        get() = if (activity?.getIsGeneralTagsEnabled() == true) {
            activity?.getGeneralTags() ?: ""
        } else {
            ""
        }

    private var _binding: FragmentCategoriesBinding? = null
    private val binding get() = _binding!!

    private var adapter: TagsCategoriesAdapter? = null
        get() = binding.categoriesRecycler.adapter as TagsCategoriesAdapter?
        set(value) {
            field = value
            binding.categoriesRecycler.adapter = value
        }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentCategoriesBinding.inflate(inflater, container, false)
        return binding.root
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val inflater = TransitionInflater.from(requireContext())
        exitTransition  = inflater.inflateTransition(R.transition.slide_right)
        enterTransition = inflater.inflateTransition(R.transition.slide_left)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initRecycler()
        initObservers()
        initUi()
    }

    private fun initUi() {
        activity


    }

    private fun initObservers() {
        homeViewModel.categories.observe(viewLifecycleOwner, {
            adapter = TagsCategoriesAdapter(it, generalTags)
        })
    }

    private fun initRecycler() = with(binding) {
        categoriesRecycler.apply {
            layoutManager = LinearLayoutManager(requireContext())
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}