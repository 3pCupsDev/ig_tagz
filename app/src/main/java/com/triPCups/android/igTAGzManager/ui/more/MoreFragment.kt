package com.triPCups.android.igTAGzManager.ui.more

import android.os.Bundle
import android.transition.TransitionInflater
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import coil.load
import com.qintong.library.InsLoadingView
import com.triPCups.android.igTAGzManager.R
import com.triPCups.android.igTAGzManager.databinding.FragmentNotificationsBinding
import com.triPCups.android.igTAGzManager.ui.settings.SettingsFragment
import org.koin.androidx.viewmodel.ext.android.viewModel

class MoreFragment : Fragment() {

    val notificationsViewModel: MoreViewModel by viewModel()

    private var _binding: FragmentNotificationsBinding? = null
    private val binding get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val inflater = TransitionInflater.from(requireContext())
        exitTransition = inflater.inflateTransition(R.transition.explode)
        enterTransition = inflater.inflateTransition(R.transition.explode)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentNotificationsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initObservers()
        initSettings()
        initUi()
    }


    private fun initSettings() {
            childFragmentManager
                .beginTransaction()
                .replace(R.id.settings_container, SettingsFragment())
                .commit()
    }

    private fun initUi() = with(binding.loadingView) {
        load(R.drawable.ic_3p_cups)
        setOnClickListener {
            //todo open story
            post {
                Toast.makeText(context, "Thank you for using IG TagZ Manager", Toast.LENGTH_SHORT)
                    .show()
            }
            //and then
            notificationsViewModel.setStoryViewState(420, true)
        }
    }

    private fun initObservers() {
        notificationsViewModel.isClicked.observe(viewLifecycleOwner, {
                isWatchedStory -> handleStoryView(isWatchedStory)
        })
    }

    private fun handleStoryView(watchedStory: Boolean?) = with(binding.loadingView) {
        status = when (watchedStory) {
            true -> {
                InsLoadingView.Status.CLICKED
            }
            false -> {
                InsLoadingView.Status.UNCLICKED
            }
            null -> {
                InsLoadingView.Status.LOADING
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}