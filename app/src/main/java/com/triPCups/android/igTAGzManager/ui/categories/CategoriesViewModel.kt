package com.triPCups.android.igTAGzManager.ui.categories

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.triPCups.android.igTAGzManager.data.TagsCategoriesRepository
import com.triPCups.android.igTAGzManager.models.TagCategory

class CategoriesViewModel(private val categoriesRepo: TagsCategoriesRepository) : ViewModel() {

    private val _categories = MutableLiveData<ArrayList<TagCategory>>().apply {
        value = categoriesRepo.get()
    }
    val categories: LiveData<ArrayList<TagCategory>> = _categories
}