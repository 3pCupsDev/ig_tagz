package com.triPCups.android.igTAGzManager.common

import android.app.AlertDialog
import android.content.Context
import android.util.Log
import android.widget.EditText

    //todo replace with 3P Cups library: Media -> EditTextDialog

object EditTextDialog {

    private var currentDialog: AlertDialog? = null

    fun showDialog(
        context: Context,
        initializedText: String = "",
        title: String = "Rename Tag",
        positiveButtonText: String = "OK",
        negativeButtonText: String = "Cancel",
        callback: (textInput: String) -> Unit
    ) {
        val editText = EditText(context)
        editText.setText(initializedText)
        currentDialog = AlertDialog.Builder(context)
            .setTitle(title)
            .setView(editText)
            .setPositiveButton(positiveButtonText) { dialogInterface, i ->
                val textInput: String = editText.text.toString()
                Log.d("wow", "editText's Input is: $textInput")
                callback(textInput)

                dialogInterface.dismiss()
            }
            .setNegativeButton(negativeButtonText, null)
            .create()
        currentDialog?.show()
    }

    fun dismiss() {
        currentDialog?.dismiss()
    }

}