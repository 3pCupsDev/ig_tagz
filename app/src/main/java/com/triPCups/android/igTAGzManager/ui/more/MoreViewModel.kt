package com.triPCups.android.igTAGzManager.ui.more

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.triPCups.android.igTAGzManager.common.AppSettings
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import kotlin.random.Random

class MoreViewModel(private val appSettings: AppSettings) : ViewModel() {

    private val _isClicked = MutableLiveData<Boolean?>().apply {
        value = null
    }
    val isClicked: LiveData<Boolean?> = _isClicked
    private val randLoadingTime
        get() = Random.nextLong(4200)

    init {
        setStoryViewState(isStoryClicked= false)
    }

    fun setStoryViewState(time: Long = randLoadingTime, isStoryClicked: Boolean? = null) =
        with(viewModelScope) {
        launch {
            setStoryViewStateCoroutine(time, isStoryClicked)
        }
    }

    private suspend fun setStoryViewStateCoroutine(time: Long = 0, isStoryClicked: Boolean? = null) =
        withContext(Dispatchers.IO) {
            delay(time)
            _isClicked.postValue(isStoryClicked)
        }
}