package com.triPCups.android.igTAGzManager.ui.settings

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.preference.*
import com.triPCups.android.igTAGzManager.R
import com.triPCups.android.igTAGzManager.common.AppSettings
import com.triPCups.android.igTAGzManager.common.AppsUtils.Companion.openAppInStore
import com.triPCups.android.igTAGzManager.common.AppsUtils.Companion.openDeveloperPage
import com.triPCups.android.igTAGzManager.common.AppsUtils.Companion.openUrl
import com.triPCups.android.igTAGzManager.data.putGeneralTags
import com.triPCups.android.igTAGzManager.data.putIsGeneralTagsEnabled
import org.koin.android.ext.android.inject

class SettingsFragment : PreferenceFragmentCompat() {

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.root_preferences, rootKey)
    }

    val appSettings: AppSettings by inject()

    companion object {
        const val GENERAL_TAGS_KEY = "general_tags"
        const val IS_GENERAL_TAGS_ENABLED_KEY = "is_general_tags_enabled"
    }

    private fun subscribeChanges() {
        val generalTags: EditTextPreference? = findPreference("general_tag") as EditTextPreference?
        generalTags?.onPreferenceChangeListener = Preference.OnPreferenceChangeListener { preference, newValue ->
                val generalTags = newValue.toString()
                activity?.putGeneralTags(generalTags)
                Log.e("wow", "generalTags: $generalTags")
                true
            }

        val isGeneralTagsEnabled: SwitchPreferenceCompat? = findPreference("sync") as SwitchPreferenceCompat?
        isGeneralTagsEnabled?.onPreferenceChangeListener = Preference.OnPreferenceChangeListener { preference, newValue ->
                activity?.putIsGeneralTagsEnabled(newValue as Boolean)
                Log.e("wow", "generalTags: $generalTags")
                true
            }
    }

    override fun onPreferenceTreeClick(preference: Preference?): Boolean {
        Log.d("wow", "onPreferenceTreeClick: ${preference?.key}")
        return when(preference?.key) {
            "general_tag" -> {
                true
            }
            "go_to_store" -> {
                context?.openAppInStore()
                true
            }
            "more_apps" -> {
                context?.openDeveloperPage()
                true
            }
            "website" -> {
                context?.openUrl("https://3p-cups.com/")
                true
            }
            else -> {
                super.onPreferenceTreeClick(preference)
            }
        }

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        subscribeChanges()
    }
}