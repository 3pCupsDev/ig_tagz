package com.triPCups.android.igTAGzManager.di

import android.content.Context
import android.content.SharedPreferences
import com.google.gson.Gson
import com.triPCups.android.igTAGzManager.common.AppSettings
import com.triPCups.android.igTAGzManager.data.TagsCategoriesRepository
import com.triPCups.android.igTAGzManager.data.TagsCategoriesRepositoryImpl
import org.koin.dsl.module

val dataModule = module {
    single { provideSharedPreferences(get()) }

    factory { provideGson() }
    factory { AppSettings(get(), get()) }


    single<TagsCategoriesRepository> { TagsCategoriesRepositoryImpl(/*get()*/) }
//    single { BusinessService() }
//    factory { AppSettings(get()) }
//
}


fun provideGson(): Gson {
    return Gson()
}


fun provideSharedPreferences(context: Context): SharedPreferences {
    return context.getSharedPreferences("User", Context.MODE_PRIVATE)
}
