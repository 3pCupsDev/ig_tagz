package com.triPCups.android.igTAGzManager.models

class Tag(
    var id: Int = 0,
    var name: String = ""
) {

    override fun toString(): String {
        return "#$name"
    }
}


class TagCategory(
    var catName: String = "",
    var tags: ArrayList<Tag> = arrayListOf()
)