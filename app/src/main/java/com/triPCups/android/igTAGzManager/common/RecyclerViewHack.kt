package com.triPCups.android.igTAGzManager.common

import androidx.recyclerview.widget.RecyclerView



/**
 * Forces RecyclerView adapter to call createViewHolder() - i.e. redraw all all items' layouts
 */
fun RecyclerView.resetAdapterState() {
    val currentAdapter = adapter
    adapter = currentAdapter
}