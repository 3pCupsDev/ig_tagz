package com.triPCups.android.igTAGzManager.adapters

import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.ContentProviderCompat.requireContext
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.HORIZONTAL
import com.google.android.flexbox.FlexboxLayoutManager
import com.triPCups.android.igTAGzManager.adapters.TagsAdapterHelper.Companion.copyTagsList
import com.triPCups.android.igTAGzManager.adapters.TagsAdapterHelper.Companion.showAddTagDialog
import com.triPCups.android.igTAGzManager.common.ClipboardUtil
import com.triPCups.android.igTAGzManager.common.EditTextDialog
import com.triPCups.android.igTAGzManager.common.resetAdapterState
import com.triPCups.android.igTAGzManager.databinding.TagCategoryItemBinding
import com.triPCups.android.igTAGzManager.models.TagCategory

class TagsCategoriesAdapter(private val tagCategoriesList: ArrayList<TagCategory>, private val generalTags: String= "") : RecyclerView.Adapter<TagsCategoriesAdapter.TagCategoryVH>() {

    init {

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TagCategoryVH {
        return TagCategoryVH(TagCategoryItemBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun onBindViewHolder(holder: TagCategoryVH, position: Int) {
        val item = tagCategoriesList[position]

        holder.bind(item)
    }

    override fun getItemCount(): Int {
        return tagCategoriesList.size
    }


    inner class TagCategoryVH(val binding: TagCategoryItemBinding) : RecyclerView.ViewHolder(binding.root) {

        private var isEditable: Boolean = false

        init {
            binding.root.setOnClickListener {
//                adapter?.copyTagsList(itemView.context, generalTags)
                onEditButtonClick()
            }

            binding.copyButton.setOnClickListener {
                adapter?.copyTagsList(itemView.context, generalTags)
            }
            binding.root.setOnLongClickListener {
                onEditButtonClick()
                true
            }
            binding.categoryTagsRecycler.setOnClickListener {
                onEditButtonClick()
            }
            binding.addTagButton.setOnClickListener {
                adapter?.showAddTagDialog(itemView.context)
            }
            initRecycler()
        }

        private fun onEditButtonClick() {
            isEditable = !isEditable
            adapter?.editable = isEditable
            binding.categoryTagsRecycler.resetAdapterState()
            binding.addTagButton.visibility = if(isEditable) {
                View.VISIBLE
            } else {
                View.GONE
            }
        }

        private var adapter: TagsAdapter? = null
            get() = binding.categoryTagsRecycler.adapter as TagsAdapter?
            set(value) {
                field = value
                binding.categoryTagsRecycler.adapter = value
            }


        private fun initRecycler() = with(binding) {
            categoryTagsRecycler.apply {
                adapter = TagsAdapter(editable = isEditable) {
                    onEditButtonClick()
                }
//                layoutManager = LinearLayoutManager(context, HORIZONTAL, false)
                layoutManager = FlexboxLayoutManager(itemView.context)
            }
        }

        fun bind(item: TagCategory) = with(binding) {
            categoryNameText.text = item.catName
            adapter?.submitList(item.tags)
        }
    }
}