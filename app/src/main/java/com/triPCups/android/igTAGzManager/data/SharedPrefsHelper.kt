package com.triPCups.android.igTAGzManager.data

import android.app.Activity
import android.content.SharedPreferences
import androidx.preference.PreferenceManager
import com.triPCups.android.igTAGzManager.ui.settings.SettingsFragment

fun Activity.putIsGeneralTagsEnabled(isEnabled: Boolean) {
    val sharedPreferences: SharedPreferences =
        PreferenceManager.getDefaultSharedPreferences(
            applicationContext
        )
    val editor = sharedPreferences.edit()
    editor.putBoolean(SettingsFragment.IS_GENERAL_TAGS_ENABLED_KEY, isEnabled)
    editor.apply()
}


fun Activity.getIsGeneralTagsEnabled(): Boolean {
    val sharedPreferences: SharedPreferences =
        PreferenceManager.getDefaultSharedPreferences(
            applicationContext
        )
    return sharedPreferences.getBoolean(SettingsFragment.IS_GENERAL_TAGS_ENABLED_KEY, false)
}

fun Activity.putGeneralTags(tags: String) {
    val sharedPreferences: SharedPreferences =
        PreferenceManager.getDefaultSharedPreferences(
            applicationContext
        )
    val editor = sharedPreferences.edit()
    editor.putString(SettingsFragment.GENERAL_TAGS_KEY, tags)
    editor.apply()
}

fun Activity.getGeneralTags(): String {
    val sharedPreferences: SharedPreferences =
        PreferenceManager.getDefaultSharedPreferences(
            applicationContext
        )

    val savedGeneralTags = sharedPreferences.getString(SettingsFragment.GENERAL_TAGS_KEY, "")
    return if (!savedGeneralTags.isNullOrEmpty()) {
        " $savedGeneralTags"
    } else {
        ""
    }
}