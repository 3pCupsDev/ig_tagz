package com.triPCups.android.igTAGzManager.data

import com.triPCups.android.igTAGzManager.models.TagCategory

class TagsCategoriesRepositoryImpl : TagsCategoriesRepository{

    override fun get(): ArrayList<TagCategory> {
        return TagsLists.getTagsCategoriesList()
    }
}

interface TagsCategoriesRepository {
    fun get(): ArrayList<TagCategory>
}