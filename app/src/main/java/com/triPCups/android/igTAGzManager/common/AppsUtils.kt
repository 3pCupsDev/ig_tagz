package com.triPCups.android.igTAGzManager.common

import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri


class AppsUtils {

    companion object {
        //todo remove and use from 3P Cups essetinals code

        //Open Market
        fun Context.openDeveloperPage() {
            val developerId = "8456795065374888880" // todo extract
            try {
                openUrl("market://dev?id=$developerId")
            } catch (ex: ActivityNotFoundException) {
                openUrl("https://play.google.com/store/apps/details?id=$developerId")
            }
        }


        //Open Market
        fun Context.openAppInStore(appPackageName: String = packageName) {
            try {
                openUrl("market://details?id=$appPackageName")
            } catch (ex: ActivityNotFoundException) {
                openUrl("https://play.google.com/store/apps/details?id=$appPackageName")
            }
        }

        fun Context.openUrl(url: String) {
            this.startActivity(
                Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse(url)
                )
            )
        }
    }
}