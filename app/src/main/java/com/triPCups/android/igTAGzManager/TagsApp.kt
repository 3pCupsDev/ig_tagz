package com.triPCups.android.igTAGzManager

import android.app.Application
import com.triPCups.android.igTAGzManager.di.appModule
import com.triPCups.android.igTAGzManager.di.dataModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.GlobalContext.startKoin
import java.util.logging.Logger

class TagsApp : Application() {

    override fun onCreate() {
        super.onCreate()

        initKoin()
    }

    private fun initKoin() {
        startKoin {
            androidContext(this@TagsApp)
            // declare modules
            modules(appModule)
            modules(dataModule)
//            logger()//todo inherit from Wow
            androidLogger()
        }
    }
}