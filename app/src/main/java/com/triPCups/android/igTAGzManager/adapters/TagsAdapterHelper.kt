package com.triPCups.android.igTAGzManager.adapters

import android.content.Context
import android.util.Log
import android.widget.Toast
import com.triPCups.android.igTAGzManager.common.ClipboardUtil
import com.triPCups.android.igTAGzManager.common.EditTextDialog
import com.triPCups.android.igTAGzManager.models.Tag

class TagsAdapterHelper {
    companion object {
        fun TagsAdapter.getList(): ArrayList<Tag> {
            return currentList.toList() as ArrayList<Tag>? ?: arrayListOf()
        }

        fun TagsAdapter.getListAsHashTagsString(): String {
            return getList().joinToString(" ")
        }

        fun TagsAdapter.showAddTagDialog(context: Context?) {
            context?.let { ctx ->
                EditTextDialog.showDialog(ctx, title= "Add Tag",  callback = {
                    addTag(it)
                })
            }
        }

        fun TagsAdapter.copyTagsList(context: Context, generalTags: String="") {
            (getListAsHashTagsString()).let { text ->
                //add specific 3P Cups tag
                val newTagsStr = "$text #3PCups\n$generalTags"
                Log.d("wow", newTagsStr)

                ClipboardUtil.copyToClipboard(context, newTagsStr) {
                    Toast.makeText(context, "Copied to Clipboard!", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }
}