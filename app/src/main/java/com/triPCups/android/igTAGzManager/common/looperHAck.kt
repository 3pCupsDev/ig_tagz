package com.triPCups.android.igTAGzManager.common

import android.content.Context
import android.os.Handler
import android.os.Looper

fun Any.post(callback: () -> Unit) {
    Handler(Looper.getMainLooper()).post {
        callback()
    }
}