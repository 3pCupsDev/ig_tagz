package com.triPCups.android.igTAGzManager.adapters

import android.content.DialogInterface
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import androidx.viewbinding.ViewBinding
import com.triPCups.android.igTAGzManager.adapters.TagsAdapterHelper.Companion.showAddTagDialog
import com.triPCups.android.igTAGzManager.common.EditTextDialog
import com.triPCups.android.igTAGzManager.databinding.SimpleTagItemBinding
import com.triPCups.android.igTAGzManager.databinding.TagItemBinding
import com.triPCups.android.igTAGzManager.models.Tag

class TagsAdapter(var editable: Boolean = true,
                  var onSimpleTextClick: (() -> Unit)? = null) :
    ListAdapter<Tag, TagsAdapter.BasicTagVH>(TagListDiffCallback()) {

    companion object {
        const val BUBBLE_TAG_ITEM = 0
        const val TEXT_TAG_ITEM = 1
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BasicTagVH {
        val inflater = LayoutInflater.from(parent.context)
        return when (viewType) {
            BUBBLE_TAG_ITEM -> {
                TagVH(TagItemBinding.inflate(inflater, parent, false))
            }
            else -> { //TEXT_TAG_ITEM
                SimpleTagVH(SimpleTagItemBinding.inflate(inflater, parent, false))
            }
        }
        /*if(editable) {
            TagVH(TagItemBinding.inflate(inflater, parent, false))
        } else {
            SimpleTagVH(SimpleTagItemBinding.inflate(inflater, parent, false))
        }*/
    }

    override fun getItemViewType(position: Int): Int {
        return if (editable) {
            BUBBLE_TAG_ITEM
        } else {
            TEXT_TAG_ITEM
        }
    }

    override fun onBindViewHolder(holder: BasicTagVH, position: Int) {
        val tag = getItem(position)
        holder.bind(tag)

            holder.itemView.setOnClickListener(holder.onClickListener)
//        if(holder is TagVH) {
//        } else if(holder is SimpleTagVH){
//        }
    }

    abstract inner class BasicTagVH(open val binding: ViewBinding) :
        RecyclerView.ViewHolder(binding.root) {
        abstract var currentText: String

        val onClickListener: View.OnClickListener = View.OnClickListener {
            if(editable) {
//                showAddTagDialog()
                EditTextDialog.showDialog(itemView.context, currentText, callback = {
                    currentText = it
                })
            } else {
                onSimpleTextClick?.invoke()
            }
        }

        fun bind(tag: Tag) = with(binding) {
            currentText = "#" + tag.name
        }
    }

    inner class SimpleTagVH(override val binding: SimpleTagItemBinding) : BasicTagVH(binding) {
        override var currentText: String = ""
            get() = ((binding.tagText.text ?: "") as String).replace("#", "")
            set(value) {
                field = value
                binding.tagText.text = value
            }
    }

    inner class TagVH(override val binding: TagItemBinding) : BasicTagVH(binding) {

        override var currentText: String = ""
            get() = ((binding.tagText.text ?: "") as String).replace("#", "")
            set(value) {
                field = value
                binding.tagText.text = value
            }
    }

    fun addTag(tag: String) {
        val wordsList = tag.split(" ")
        val newTagsArr: ArrayList<Tag> = arrayListOf()
        for (wordTag in wordsList) {
            val newTag = Tag(id = itemCount + 1, name = wordTag.replace("#", ""))
            newTagsArr.add(newTag)
        }
        addTags(newTagsArr)
    }

    fun addTags(tags: ArrayList<Tag>) {
        (currentList.toList() as ArrayList<Tag>?)?.let { originalList ->
            originalList.addAll(tags)
            submitList(originalList)
        }
    }

    fun addTag(tag: Tag) {
        (currentList.toList() as ArrayList<Tag>?)?.let { originalList ->
            originalList.add(tag)
            submitList(originalList)
        }
    }

}

class TagListDiffCallback : DiffUtil.ItemCallback<Tag>() {
    override fun areItemsTheSame(oldItem: Tag, newItem: Tag): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: Tag, newItem: Tag): Boolean {
        return oldItem.name == newItem.name
    }
}