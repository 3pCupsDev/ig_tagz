package com.triPCups.android.igTAGzManager.ui.myTags

import android.os.Bundle
import android.transition.TransitionInflater
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.google.android.flexbox.FlexboxLayoutManager
import com.triPCups.android.igTAGzManager.R
import com.triPCups.android.igTAGzManager.adapters.TagsAdapter
import com.triPCups.android.igTAGzManager.adapters.TagsAdapterHelper.Companion.copyTagsList
import com.triPCups.android.igTAGzManager.adapters.TagsAdapterHelper.Companion.showAddTagDialog
import com.triPCups.android.igTAGzManager.data.getGeneralTags
import com.triPCups.android.igTAGzManager.data.getIsGeneralTagsEnabled
import com.triPCups.android.igTAGzManager.databinding.FragmentMyTagsBinding
import com.triPCups.android.igTAGzManager.ui.categories.CategoriesViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel


class MyTagsFragment : Fragment() {

    val dashboardViewModel: MyTagsViewModel by viewModel()

    private var _binding: FragmentMyTagsBinding? = null
    private val binding get() = _binding!!

    private val generalTags: String
        get() = if (activity?.getIsGeneralTagsEnabled() == true) {
            activity?.getGeneralTags() ?: ""
        } else {
            ""
        }
    private var adapter: TagsAdapter? = null
        get() = binding.tagsListRecycler.adapter as TagsAdapter?
        set(value) {
            field = value
            binding.tagsListRecycler.adapter = value
        }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val inflater = TransitionInflater.from(requireContext())
        enterTransition = inflater.inflateTransition(R.transition.slide_right)
        exitTransition = inflater.inflateTransition(R.transition.slide_left)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        //init binding
        _binding = FragmentMyTagsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initRecycler()
        initObservers()
        initUi()
    }

    private fun initUi() = with(binding) {
        copyButton.setOnClickListener {
            adapter?.copyTagsList(requireContext(), generalTags)
        }
        addBtn.setOnClickListener {
            adapter?.showAddTagDialog(context)
        }
    }

    private fun initObservers() {
        dashboardViewModel.myTagsLiveData.observe(viewLifecycleOwner, {
            adapter?.submitList(it)
        })
    }

    private fun initRecycler() = with(binding) {
        adapter = TagsAdapter()
        tagsListRecycler.apply {
            layoutManager = FlexboxLayoutManager(requireContext())
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}