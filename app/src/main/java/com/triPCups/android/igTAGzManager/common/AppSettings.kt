package com.triPCups.android.igTAGzManager.common

import android.content.SharedPreferences
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

private const val MODEL_OBJECT = "MODEL_OBJECT"
private const val MODEL_LIST = "MODEL_LIST"
private const val BOOLEAN_OBJECT = "BOOLEAN_OBJECT"
private const val OBJECT_LIST_BY_ID = "OBJECT_LIST_BY_ID"
private const val INT_OBJECT = "INT_OBJECT"

private const val FIRST_TIME_IN_APP = "FIRST_TIME_IN_APP"
private const val IS_SIGNED_IN = "SIGNED_IN"
private const val I_DONT_WANT_SIGNED_IN = "I_DONT_WANT_SIGNED_IN"


class AppSettings(private val sharedPreferences: SharedPreferences, private val gson: Gson) {


    fun <T> setModel(t: T) {
        val json = gson.toJson(t)
        sharedPreferences.edit().putString(MODEL_OBJECT, json).apply()
    }

    fun getModel(): Any? {
        val jsonPreferences = sharedPreferences.getString(MODEL_OBJECT, "")

        val type = object : TypeToken<Any>() {}.type
        return gson.fromJson<Any>(jsonPreferences, type)
    }

    fun <T> setModelList(list: List<T>) {
        val json = gson.toJson(list)
        sharedPreferences.edit().putString(MODEL_LIST, json).apply()
    }

    fun getModelList(): ArrayList<Any> {
        val jsonPreferences = sharedPreferences.getString(MODEL_LIST, "")
        val channels: ArrayList<Any> = ArrayList()

        val type = object : TypeToken<List<Any>>() {}.type
        if (jsonPreferences != "")
            channels.addAll(gson.fromJson<List<Any>>(jsonPreferences, type))

        return channels
    }

    fun setBoolean() {
        sharedPreferences.edit().putBoolean(BOOLEAN_OBJECT, false).apply()
    }

    fun getBoolean(): Boolean {
        return sharedPreferences.getBoolean(BOOLEAN_OBJECT, true)
    }

    fun getObjectListById(id: String?): ArrayList<String> {
        val jsonPreferences = if(id != null){
            sharedPreferences.getString(OBJECT_LIST_BY_ID + id, "")
        } else {
            sharedPreferences.getString(OBJECT_LIST_BY_ID, "")
        }

        val keywordsList: ArrayList<String> = ArrayList()

        val type = object : TypeToken<List<String>>() {}.type
        if (jsonPreferences != "")
            keywordsList.addAll(gson.fromJson<List<String>>(jsonPreferences, type))

        return keywordsList
    }

    fun <T> setObjectListById(list: List<T>, id: String?) {
        val json = gson.toJson(list)
        sharedPreferences.edit().putString(OBJECT_LIST_BY_ID + id, json).apply()
    }

//    // HashSet<Pair< String , Channel.Item>>
//    fun <T> setRssSources(list: HashSet<T>) {
//        val json = gson.toJson(list)
//        sharedPreferences.edit().putString(RSS_SOURCES_LIST, json).apply()
//    }
//
//    fun getRssSources(): HashSet<Pair<String, Channel.Item>> {
//        val jsonPreferences = sharedPreferences.getString(RSS_SOURCES_LIST, "")
//        val channels: HashSet<Pair<String, Channel.Item>> = HashSet()
//
//        val type = object : TypeToken<HashSet<Pair<String, Channel.Item>>>() {}.type
//        if (jsonPreferences != "")
//            channels.addAll(gson.fromJson<HashSet<Pair<String, Channel.Item>>>(jsonPreferences, type))
//
//        return channels
//    }

    fun setIntObject(prefKey: Int) {
        sharedPreferences.edit().putInt(INT_OBJECT, prefKey).apply()
    }

    fun getIntObject(): Int {
        return sharedPreferences.getInt(INT_OBJECT, 420) //TODO:: set defaultValue, change 420
    }


    fun setFirstTimeInApp() {
        sharedPreferences.edit().putBoolean(FIRST_TIME_IN_APP, false).apply()
    }

    fun getFirstTimeInApp(): Boolean {
        return sharedPreferences.getBoolean(FIRST_TIME_IN_APP, true)
    }

    fun setIsSignedIn() {
        sharedPreferences.edit().putBoolean(IS_SIGNED_IN, true).apply()
    }

    fun getIsSignedIn(): Boolean {
        return sharedPreferences.getBoolean(IS_SIGNED_IN, false)
    }


    fun setDontWantSignedIn(askMeAgain: Boolean) {
        sharedPreferences.edit().putBoolean(I_DONT_WANT_SIGNED_IN, askMeAgain).apply()
    }

    fun getDontWantSignedIn(): Boolean {
        return sharedPreferences.getBoolean(I_DONT_WANT_SIGNED_IN, false)
    }

}
