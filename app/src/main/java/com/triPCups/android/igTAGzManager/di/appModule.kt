package com.triPCups.android.igTAGzManager.di

import com.triPCups.android.igTAGzManager.ui.categories.CategoriesViewModel
import com.triPCups.android.igTAGzManager.ui.more.MoreViewModel
import com.triPCups.android.igTAGzManager.ui.myTags.MyTagsViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val appModule = module {

    // Categories Screen
    viewModel { CategoriesViewModel(get()) }

    // My Tags Screen
    viewModel { MyTagsViewModel(get()) }

    // More Screen
    viewModel { MoreViewModel(get()) }
}