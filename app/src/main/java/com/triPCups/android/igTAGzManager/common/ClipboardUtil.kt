package com.triPCups.android.igTAGzManager.common

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context

class ClipboardUtil  {

    companion object {
        fun copyToClipboard(context: Context?, text: String, onCopied: () -> Unit) {
            context?.let{ ctx->
                (ctx.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager?)?.let{ clipboard ->
                    val clip = ClipData.newPlainText(text, text)
                    clipboard.setPrimaryClip(clip)
                    onCopied()
                }
            }
        }
    }
}