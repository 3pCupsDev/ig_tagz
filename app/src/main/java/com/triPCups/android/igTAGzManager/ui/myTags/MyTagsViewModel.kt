package com.triPCups.android.igTAGzManager.ui.myTags


import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.triPCups.android.igTAGzManager.common.AppSettings
import com.triPCups.android.igTAGzManager.data.TagsLists
import com.triPCups.android.igTAGzManager.models.Tag

class MyTagsViewModel(private val appSettings: AppSettings) : ViewModel() {

    private val _myTagsData = MutableLiveData<ArrayList<Tag>>().apply {
        value = TagsLists.getDefaultMyTagsList()
    }
    val myTagsLiveData: LiveData<ArrayList<Tag>> = _myTagsData
}